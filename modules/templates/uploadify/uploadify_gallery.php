<?php
/*
Uploadify
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/

// Define a destination
include "../../../inc/connect_db.php";
include "../../../inc/function.cms.php";
session_start();
if (getUserType()<>2) die();

ini_set("memory_limit","500M");
$targetFolder = '/uploads'; // Relative to the root
$targetFolder = $_POST['folder'].'/'.$_POST['id_type'];

$verifyToken = md5('unique_salt' . $_POST['timestamp']);
$id=intval(mysql_result(mysql_query("SELECT max(id) FROM `{$pref}db_gallery` "),0, 'max(id)'));
$id++;
$file = md5('unique_salt' . $_POST['timestamp'] . $_POST['id_type'] . $_POST['id_el'] . $id . substr(md5(rand(100,800)),0,5+rand(1,2)) );

if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
	$tempFile = $_FILES['Filedata']['tmp_name'];
	if ($tempFile){
		$targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
		$targetFile = rtrim($targetPath,'/') . '/' . $_FILES['Filedata']['name'];
		$targetFile = rtrim($targetPath,'/') . '/'.$file;
		
		// Validate the file type
		$fileTypes = array('jpg','jpeg','gif','png','JPG','JPEG','GIF','PNG'); // File extensions
		$fileParts = pathinfo($_FILES['Filedata']['name']);
		$pic = $file.'.'.$fileParts['extension'];
		if (in_array($fileParts['extension'],$fileTypes)) {
			mysql_query("insert into {$pref}db_gallery set pid='{$_POST['pid']}',title='{$_POST['title']}',img='{$pic}',sort='0',status='1'");
			move_uploaded_file($tempFile,$targetFile.'.'.$fileParts['extension']);

			if (!empty($_POST['editsize'])){
				list($w_i, $h_i, $type) = getimagesize($targetFile.'.'.$fileParts['extension']);
				if ($w_i>$h_i && $w_i>880)
				resize($targetFile.'.'.$fileParts['extension'], $targetFile.'.'.$fileParts['extension'], 880,0, $percent = false, '');
				elseif ($h_i>880)
				resize($targetFile.'.'.$fileParts['extension'], $targetFile.'.'.$fileParts['extension'], 0,880, $percent = false, '');
			}	
			// else
			// resize($targetFile.'.'.$fileParts['extension'], $targetFile.'.'.$fileParts['extension'], $w_i,$h_i, $percent = false, 'jpeg');
			// unlink($targetFile.'_temp.'.$fileParts['extension']);


			echo '1';
		} else {
			echo 'Ошибка, неверный тип.';
		}
	} else {
		echo 'Ошибка загрузки.';
	}
}
?>
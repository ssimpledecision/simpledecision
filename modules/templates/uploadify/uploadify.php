<?php
/*
Uploadify
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/

// Define a destination
include "../../../inc/connect_db.php";
include "../../../inc/function.cms.php";
session_start();
if (getUserType()<>2) die();
$pos = strpos($_SERVER['SERVER_NAME'], 'www.');
if ($pos === false) $s_adr=$_SERVER['SERVER_NAME'];
else
$s_adr=substr ($_SERVER['SERVER_NAME'], 4, strlen($_SERVER['SERVER_NAME'])-4);

ini_set("memory_limit","500M");
$targetFolder = '/uploads'; // Relative to the root
$targetFolder = $_POST['folder'].'/'.$_POST['id_type'];

$verifyToken = md5('unique_salt' . $_POST['timestamp']);
$id=intval(mysql_result(mysql_query("SELECT max(id) FROM `{$pref}db_pic` "),0, 'max(id)'));
$id++;
$file = md5('unique_salt' . $_POST['timestamp'] . $_POST['id_type'] . $_POST['id_el'] . $id . substr(md5(rand(100,800)),0,5+rand(1,2)) );

if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
	$tempFile = $_FILES['Filedata']['tmp_name'];
	if ($tempFile){
		$targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
		$targetFile = rtrim($targetPath,'/') . '/' . $_FILES['Filedata']['name'];
		$targetFile = rtrim($targetPath,'/') . '/'.$file;
		
		// Validate the file type
		$fileTypes = array('jpg','jpeg','gif','png','JPG','JPEG','GIF','PNG'); // File extensions
		$fileParts = pathinfo($_FILES['Filedata']['name']);
		$pic = $file.'.'.$fileParts['extension'];
		if (in_array($fileParts['extension'],$fileTypes)) {
			mysql_query("insert into {$pref}db_pic set id_type='{$_POST['id_type']}',id_el='{$_POST['id_el']}',pic='{$pic}',domain='{$s_adr}',sort='0'");
			$insert_id=mysql_insert_id();
			move_uploaded_file($tempFile,$targetFile.'.'.$fileParts['extension']);
			if (!empty($_POST['editsize'])){
				list($w_i, $h_i, $type) = getimagesize($targetFile.'.'.$fileParts['extension']);
				if ($w_i>$h_i && $w_i>880)
				resize($targetFile.'.'.$fileParts['extension'], $targetFile.'.'.$fileParts['extension'], 880,0, $percent = false, '');
				elseif ($h_i>880)
				resize($targetFile.'.'.$fileParts['extension'], $targetFile.'.'.$fileParts['extension'], 0,880, $percent = false, '');
			}	
			echo '<tr id="trpic'.$insert_id.'">
						<td>'.$insert_id.'</td>
						<td><img src="/resize/?pic=../files/images/'.$_POST['id_type'].'/'.$pic.'&w=100&h=100&tp=2"></td>
						<td><input class="sortpic" id="'.$insert_id.'" type="text" size="3" value="0"></td>
						<td><input class="deletepic" id="'.$insert_id.'" type="image" src="../templates/img/icn_trash.png" title="Удалить"></td>
					</tr>';
		} else {
			echo 'Ошибка, неверный тип.';
		}
	} else {
		echo 'Ошибка загрузки.';
	}
}
?>
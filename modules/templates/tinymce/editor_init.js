	tinyMCE.init({
		// General options
		mode : "exact",
		elements : "elm1,elm2,elm3,elm4",
		theme : "advanced",
		plugins : "imagemanager,filemanager,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",
		language : 'ru',
		height : "400",
		convert_urls : false,
		absolute_urls : true,
		relative_urls : false,

		// Theme options
		theme_advanced_buttons1 : "newdocument,preview,fullscreen,print,|,cut,copy,paste,pastetext,pasteword,|,cleanup,|,undo,redo,|,search,replace,|,visualchars,code",
		theme_advanced_buttons2 : "image,link,unlink,anchor,|,insertdate,inserttime,blockquote,|,cite,abbr,acronym,del,ins,attribs,|,nonbreaking,pagebreak,|,hr,removeformat,visualaid,|,charmap,emotions,media,advhr,|,ltr,rtl",
		theme_advanced_buttons3 : "tablecontrols,|,insertlayer,moveforward,movebackward,absolute,|,styleprops,|,sub,sup,|,outdent,indent",
		theme_advanced_buttons4 : "styleselect,formatselect,fontselect,fontsizeselect,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,forecolor,backcolor",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// file_browser_callback : "tinyBrowser",
		// Example content CSS (should be your site CSS)
		// content_css : "../templates/tmpl1/css/style.css",

		// Drop lists for link/image/media/template dialogs
		// template_external_list_url : "lists/template_list.js",
		// external_link_list_url : "lists/link_list.js",
		// external_image_list_url : "lists/image_list.js",
		// media_external_list_url : "lists/media_list.js",
		extended_valid_elements : "iframe[name|src|framespacing|border|frameborder|scrolling|title|height|width],object[declare|classid|codebase|data|type|codetype|archive|standby|height|width|usemap|name|tabindex|align|border|hspace|vspace]",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});

$(document).ready(function(){
	$('#elm1').after('<br>&nbsp;&nbsp;&nbsp;&nbsp;Визуальный редактор<input type="checkbox" checked id="tinyeditinitelm1" />');
	$('#tinyeditinitelm1').live('change',function(){
		if ($(this).attr('checked')=='checked'){
		 	tinyMCE.execCommand("mceAddControl", true, "elm1");
		} else {
	    	tinyMCE.execCommand('mceFocus', false, 'elm1');
	    	tinyMCE.execCommand('mceRemoveControl', false, 'elm1');
		}
	});
});

<?$tpl_admin_version='1.3';?>
<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<title><?=$s_title?> | Система управления сайтом Simple Decision</title>
	<?include $pt.$cmspatch.'/templates/script.php';?>
	
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="<?=$pt.$tpl_admin?>/css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="<?=$pt.$tpl_admin?>/js/hideshow.js" type="text/javascript"></script>
	<script src="<?=$pt.$tpl_admin?>/js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?=$pt.$tpl_admin?>/js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>
    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>

</head>


<body>
<?if ($cms_ver<$tpl_admin_version)print '<font color="red"><b>Версия шаблона не совпадает с версией CMS</b></font><br>';?>

	<header id="header">
		<hgroup>
			<h1 class="site_title">SD CMS <?=$cms_ver?></h1>
			<h2 class="section_title"><?=(!empty($mod_title[$modul]))? $mod_title[$modul] : 'Система управления';?></h2><div class="btn_view_site"><a href="?logout">Выход</a></div><div class="btn_view_site"><a href="/" target="_blanck">Сайт</a></div>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<p><?=getUser($_SESSION['user_id'],'name')?> (<a href="#">3 Messages</a>)</p>
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="/<?=$cmspatch?>">Система управления</a> <div class="breadcrumb_divider"></div> <a <? if ($modul!=substr($href, 9)) echo "href='/$cmspatch/$modul'";?> class="current"><?=$mod_title[$modul]?></a>#breadcrumb#</article>
		</div>
	</section><!-- end of secondary bar -->
	
	<aside id="sidebar" class="column">
		<form class="quick_search" action="" method="POST">
			<input type="submit" style="width:0px;height:0px;position:absolute;left:-10000px;"/>
			<input type="text" name="searchinmodul" placeholder="Поиск" />
		</form>
		<? include $pt.$tpl_admin."/menu.php"; ?>
		
		<footer>
			<hr />
			<p><strong>Copyright &copy; 2012 - <? echo date("Y");?> Simple Decision CMS</strong></p>
			<p>Download  <a href="https://bitbucket.org/ssimpledecision/simpledecision/get/master.zip">last version CMS</a></p>
		</footer>
	</aside><!-- end of sidebar -->
	
	<section id="main" class="column">

-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Май 30 2013 г., 00:27
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `mydb`
--

-- --------------------------------------------------------

--
-- Структура таблицы `pref_db_components`
--

DROP TABLE IF EXISTS `pref_db_components`;
CREATE TABLE IF NOT EXISTS `pref_db_components` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `comp` varchar(255) NOT NULL,
  `ptype` varchar(250) NOT NULL,
  `pid` varchar(250) NOT NULL,
  `exception` varchar(250) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '-1',
  `sort` int(11) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `tmpl` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ptype` (`ptype`),
  KEY `pid` (`pid`),
  KEY `exception` (`exception`),
  KEY `status` (`status`),
  KEY `sort` (`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pref_db_modules`
--

DROP TABLE IF EXISTS `pref_db_modules`;
CREATE TABLE IF NOT EXISTS `pref_db_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `table` varchar(255) NOT NULL,
  `instal` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9002 ;

--
-- Дамп данных таблицы `pref_db_modules`
--

INSERT INTO `pref_db_modules` (`id`, `name`, `title`, `table`, `instal`, `status`) VALUES
(2001, 'banners', 'Банеры', 'db_banners', 0, 2),
(2002, 'forum', 'Форум', 'db_forum', 0, 2),
(2003, 'gallery', 'Галерея', 'db_gallery', 0, 2),
(2004, 'news', 'Новости', 'db_news', 0, 2),
(1001, 'structure', 'Структура', 'db_structure', 1, 1),
(1002, 'users', 'Полльзователи', 'db_users', 1, 1),
(1003, 'components', 'Компоненты', 'db_components', 1, 1),
(9001, 'modules', 'Модули', 'db_components', 1, 1),
(8001, 'logs', 'Логи', 'db_logs', 0, 2),
(8002, 'pic', 'Изображения', 'db_pic', 0, 2),
(8003, 'rate', 'Рейтинг', 'db_rate', 0, 2),
(8004, 'session', 'Сессии', 'db_session', 0, 2),
(8005, 'statistics', 'Статистика', 'db_statistics', 0, 2),
(8006, 'comment', 'Комментарии', 'db_comment', 0, 2),
(2005, 'catalog', 'Каталог', 'db_catalog', 0, 2),
(2006, 'materials', 'Материалы', 'db_materials', 0, 2),
(2007, 'menu', 'Меню', 'db_menu', 0, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `pref_db_structure`
--

DROP TABLE IF EXISTS `pref_db_structure`;
CREATE TABLE IF NOT EXISTS `pref_db_structure` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) NOT NULL,
  `parent` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `menu_name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `anons` text NOT NULL,
  `text` text NOT NULL,
  `type` int(11) NOT NULL,
  `href` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '-1',
  `sort` int(11) NOT NULL DEFAULT '0',
  `title` text NOT NULL,
  `description` text NOT NULL,
  `keyword` text NOT NULL,
  `menu` varchar(255) NOT NULL,
  `menu1` int(11) NOT NULL DEFAULT '0',
  `menu2` int(11) NOT NULL DEFAULT '0',
  `menu3` int(11) NOT NULL DEFAULT '0',
  `menu4` int(11) NOT NULL DEFAULT '0',
  `menu4_par` int(11) NOT NULL DEFAULT '0',
  `catalog` bigint(20) NOT NULL DEFAULT '0',
  `materials` bigint(20) NOT NULL DEFAULT '0',
  `catalog2` varchar(255) NOT NULL,
  `materials2` varchar(255) NOT NULL,
  `access_admin` varchar(255) NOT NULL,
  `access_moder` varchar(255) NOT NULL,
  `access_user` varchar(255) NOT NULL,
  `access_notuser` int(11) NOT NULL DEFAULT '0',
  `tpl` varchar(255) NOT NULL DEFAULT '0',
  `user` bigint(20) NOT NULL DEFAULT '0',
  `date1` datetime NOT NULL,
  `date2` datetime NOT NULL,
  `error` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `parent` (`parent`),
  KEY `type` (`type`),
  KEY `status` (`status`),
  KEY `sort` (`sort`),
  KEY `catalog` (`catalog`),
  KEY `materials` (`materials`),
  KEY `user` (`user`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `pref_db_structure`
--

INSERT INTO `pref_db_structure` (`id`, `pid`, `parent`, `name`, `menu_name`, `alias`, `anons`, `text`, `type`, `href`, `status`, `sort`, `title`, `description`, `keyword`, `menu`, `menu1`, `menu2`, `menu3`, `menu4`, `menu4_par`, `catalog`, `materials`, `catalog2`, `materials2`, `access_admin`, `access_moder`, `access_user`, `access_notuser`, `tpl`, `user`, `date1`, `date2`, `error`) VALUES
(1, 0, 1, 'Главная', '', '', '', '', 2, '/', 1, 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '0', '0', '0', 1, '0', 1, '2013-05-29 00:00:45', '2013-05-29 00:00:45', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `pref_db_structure_alias`
--

DROP TABLE IF EXISTS `pref_db_structure_alias`;
CREATE TABLE IF NOT EXISTS `pref_db_structure_alias` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_type` int(11) NOT NULL,
  `id_el` bigint(20) NOT NULL,
  `id_page` bigint(20) NOT NULL,
  `href` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_type` (`id_type`),
  KEY `id_el` (`id_el`),
  KEY `id_page` (`id_page`),
  KEY `href` (`href`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `pref_db_structure_alias`
--

INSERT INTO `pref_db_structure_alias` (`id`, `id_type`, `id_el`, `id_page`, `href`) VALUES
(1, 1001, 1, 1, '/');

-- --------------------------------------------------------

--
-- Структура таблицы `pref_db_users`
--

DROP TABLE IF EXISTS `pref_db_users`;
CREATE TABLE IF NOT EXISTS `pref_db_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `nik` varchar(250) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `password_new` varchar(32) NOT NULL,
  `email` varchar(250) NOT NULL,
  `phone` varchar(250) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `group` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '-1',
  `hash_id` varchar(32) NOT NULL,
  `confirm` int(11) NOT NULL DEFAULT '0',
  `about` text NOT NULL,
  `subscript` text NOT NULL,
  `subscription` int(11) NOT NULL DEFAULT '0',
  `region` bigint(20) NOT NULL DEFAULT '0',
  `page` bigint(20) NOT NULL DEFAULT '0',
  `balance` double NOT NULL DEFAULT '0',
  `options` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `status` (`status`),
  KEY `page` (`page`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pref_db_users_mes`
--

DROP TABLE IF EXISTS `pref_db_users_mes`;
CREATE TABLE IF NOT EXISTS `pref_db_users_mes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user` bigint(20) NOT NULL,
  `user_from` bigint(20) NOT NULL,
  `title` varchar(250) NOT NULL,
  `text` text NOT NULL,
  `date` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `user_from` (`user_from`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `pref_db_vars`
--

DROP TABLE IF EXISTS `pref_db_vars`;
CREATE TABLE IF NOT EXISTS `pref_db_vars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `pref_db_vars`
--

INSERT INTO `pref_db_vars` (`id`, `name`, `key`, `value`) VALUES
(1, 'Название', 'title', 'Название сайта'),
(2, '', 'version', '1.3'),
(3, 'Шаблон админки', 'templates_admin', 'admin_template'),
(4, 'Шаблон сайта', 'templates_user', 'default');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

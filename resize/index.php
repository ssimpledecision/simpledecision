<?
ini_set("memory_limit","500M");

$q = 100;
$available_filetypes = array(
	'image/jpg',
	'image/jpeg',
	'image/gif',
	'image/png',
	'image/x-png',
	);

$file_path = dirname(__FILE__);
if(isset($_GET['pic']) )
{
	$pic = $_GET['pic'];
	// $type = image_type_to_extension($pic);
	$type=explode('.', $pic);
	$type=$type[3];
	// echo $type;
	switch ($type) {
		case 'jpg':
			$src   = imagecreatefromjpeg($pic);
			break;
		case 'jpeg':
			$src   = imagecreatefromjpeg($pic);
			break;
		case 'gif':
			$src   = imagecreatefromgif($pic);
			break;
		case 'png':
			$src   = imagecreatefrompng($pic);
			break;
		case 'JPG':
			$src   = imagecreatefromjpeg($pic);
			break;
		case 'JPEG':
			$src   = imagecreatefromjpeg($pic);
			break;
		case 'GIF':
			$src   = imagecreatefromgif($pic);
			break;
		case 'PNG':
			$src   = imagecreatefrompng($pic);
			break;
		
		default:
			$src   = imagecreatefromjpeg($pic);
			break;
	}
	// $src   = imagecreatefrompng($pic);
	$src_w = imagesx($src);
	$src_h = imagesy($src);
	$cropp_w = isset($_GET['w'])?intval($_GET['w']):$src_w;
	$cropp_h = isset($_GET['h'])?intval($_GET['h']):$src_h;
	$transform_type = isset($_GET['tp'])?intval($_GET['tp']):1;

	switch ($transform_type)
	{
		case '1':
			$new_width    = $cropp_w;
			$new_height   = $src_h*$new_width/$src_w;
			$offset_x = 0;
			$offset_y = 0;
			if($cropp_h >= $new_height)
			{
				$new_height = $cropp_h;
				$new_width  = $src_w*$new_height/$src_h;
			}
			$offset_x = ($new_width - $cropp_w)/2;
			$offset_y = ($new_height - $cropp_h)/2;
			$cropp_h = ($cropp_h>=$new_height)?$new_height:$cropp_h;
			$tmp_image = imagecreatetruecolor($new_width, $new_height);
			$dest      = imagecreatetruecolor($cropp_w, $cropp_h);
			imagecopyresampled($tmp_image, $src, 0, 0, 0, 0, $new_width, $new_height, $src_w, $src_h);
			imagecopy($dest, $tmp_image, 0, 0, $offset_x, $offset_y, $cropp_w, $cropp_h);
		break;
		case '2':
			if($src_w >= $src_h)
			{
				$new_width    = $cropp_w;
				$new_height   = $src_h*$new_width/$src_w;
				$cropp_h = ($cropp_h>=$new_height)?$new_height:$cropp_h;
				$tmp_image = imagecreatetruecolor($new_width, $new_height);
				$dest      = imagecreatetruecolor($cropp_w, $cropp_h);
				imagecopyresampled($tmp_image, $src, 0, 0, 0, 0, $new_width, $new_height, $src_w, $src_h);
				imagecopy($dest, $tmp_image, 0, 0, 0, 0, $cropp_w, $cropp_h);
			}
			else
			{
				$new_height = $cropp_h;
				$new_width  = $src_w*$new_height/$src_h;
				$dest       = imagecreatetruecolor($new_width, $new_height);
				imagecopyresampled($dest, $src, 0, 0, 0, 0, $new_width, $new_height, $src_w, $src_h);
			}
		break;
		case '3':
			$new_width    = $cropp_w;
			$new_height   = $src_h*$new_width/$src_w;
			$offset_x = 0;
			$offset_y = 0;
			if($cropp_h >= $new_height)
			{
				$new_height = $cropp_h;
				$new_width  = $src_w*$new_height/$src_h;
			}
			$offset_x = ($new_width - $cropp_w)/2;
			$offset_y = ($new_height - $cropp_h)/2;
			$cropp_h = ($cropp_h>=$new_height)?$new_height:$cropp_h;
			$tmp_image = imagecreatetruecolor($new_width, $new_height);
			$dest      = imagecreatetruecolor($cropp_w, $cropp_h);
			imagecopyresampled($tmp_image, $src, 0, 0, 0, 0, $new_width, $new_height, $src_w, $src_h);
			imagecopy($dest, $tmp_image, 0, 0, 0, 0, $cropp_w, $cropp_h);
		break;
		default:
			$new_width    = $cropp_w;
			$new_height   = $src_h*$new_width/$src_w;
			$cropp_h = ($cropp_h>=$new_height)?$new_height:$cropp_h;
			$tmp_image = imagecreatetruecolor($new_width, $new_height);
			$dest      = imagecreatetruecolor($cropp_w, $cropp_h);
			imagecopyresampled($tmp_image, $src, 0, 0, 0, 0, $new_width, $new_height, $src_w, $src_h);
			imagecopy($dest, $tmp_image, 0, 0, 0, 0, $cropp_w, $cropp_h);
		break;
	}
	header("Content-type: image/jpeg");
	imagejpeg($dest,'',$q);
	imagedestroy($dest);
	imagedestroy($tmp_image);
	imagedestroy($src);
}
else
{
	$src = imagecreatefromjpeg ($file_path.'../files/images/default.jpg');
	header("Content-type: image/jpeg");
	imagejpeg($src,'',$q);
}